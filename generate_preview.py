from glob import glob
from json import dump, load
from os import popen, path

# Iterate over games within ./games/*.json
game_previews = []
game_file_paths = glob("./games/*.json")
for game_file_path in game_file_paths:
    print(f">> Generating preview for '{game_file_path}'")

    filename = path.basename(game_file_path)
    game_preview = { "filename": filename }

    print(f"'{game_file_path}' extracted filename: '{filename}'")

    # Retrieve Git last updated timestamp 
    git_updated_timestamp_str = popen(f'git log -1 --pretty="format:%at" "{game_file_path}"').read()
    # Ignore files with length 0 - somehow not added?
    if(len(git_updated_timestamp_str) == 0): 
        print(f"/!\\ '{game_file_path}' couldn't retrieve git updated timestamp")
        continue
    git_updated_timestamp = int(git_updated_timestamp_str)

    # Parse title, version, and synopsis from file
    game_preview["updated"] = git_updated_timestamp
    with open(game_file_path, "r") as game_file:
        game_data = load(game_file)

        # Skip adding game data if somehow missing information field
        if game_data["data"] == None or game_data["data"]["information"] == None:
            print(f"/!\\ '{game_file_path}' couldn't find information for preview generation")

        game_preview["engine"] = game_data["version"] if "version" in game_data else "0.0.0"
        game_preview["title"] = game_data["data"]["information"]["title"]
        game_preview["author"] = game_data["data"]["information"]["author"]
        game_preview["synopsis"] = game_data["data"]["information"]["synopsis"]
        game_preview["version"] = game_data["data"]["information"]["version"]

        print(f"'{game_file_path}' found '{game_preview['title']}' by '{game_preview['author']}' with version '{game_preview['version']}'")

    game_previews.append(game_preview)

# Sort preview with most recently updated first (largest timestamp)
print(f">> Sorting game previews by most recently updated")
game_previews_sorted = sorted(game_previews, key=lambda x: x["updated"])[::-1]

# Write resulting sorted preview data to preview.json
with open("preview.json", "w") as preview_file:
    print(f">> Writing game previews to preview.json")
    dump(game_previews, preview_file)
